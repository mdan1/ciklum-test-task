package com.trendyol.services;

import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.repository.LinkRepository;
import com.trendyol.services.converter.ConverterFactory;
import com.trendyol.services.converter.HomeUrlConverter;
import com.trendyol.services.converter.ProductUrlConverter;
import com.trendyol.services.converter.SearchUrlConverter;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LinkConverterServiceTest {

    @InjectMocks
    LinkConverterService linkConverterService;

    @Mock
    ConverterFactory converterFactory;

    @Mock
    LinkRepository linkRepository;

    Map<String, String> productMap;
    Map<String, String> searchMap;
    Map<String, String> homeMap;

    @Before
    public void before() {
        prepareProductLinks();
        prepareSearchLinks();
    }

    @Test
    public void toDeepLinkTest() {
        when(converterFactory.getUrlConverter(any(UriComponents.class))).thenReturn(new ProductUrlConverter());
        when(linkRepository.save(any(Link.class))).thenAnswer(i -> i.getArguments()[0]);

        productMap.forEach((key, expectedResult) -> {
            var linkDto = LinkDto.builder().webUrl(key).build();
            var actualResult = linkConverterService.getDeepLink(linkDto).getDeepLink();
            Assert.assertEquals(expectedResult, actualResult);
        });

        when(converterFactory.getUrlConverter(any(UriComponents.class)))
                .thenReturn(new SearchUrlConverter());

        searchMap.forEach((key, expectedResult) -> {
            var linkDto = LinkDto.builder().webUrl(key).build();
            var actualResult = linkConverterService.getDeepLink(linkDto).getDeepLink();
            Assert.assertEquals(expectedResult, actualResult);
        });

        when(converterFactory.getUrlConverter(any(UriComponents.class)))
                .thenReturn(new HomeUrlConverter());

        prepareHomeWebUrlLinks();
        homeMap.forEach((key, expectedResult) -> {
            var linkDto = LinkDto.builder().webUrl(key).build();
            var actualResult = linkConverterService.getDeepLink(linkDto).getDeepLink();
            Assert.assertEquals(expectedResult, actualResult);
        });
    }

    @Test
    public void toWebUrlTest() {
        when(converterFactory.getUrlConverter(any(UriComponents.class))).thenReturn(new ProductUrlConverter());
        when(linkRepository.save(any(Link.class))).thenAnswer(i -> i.getArguments()[0]);

        productMap.forEach((expectedResult, value) -> {
            var linkDto = LinkDto.builder().deepLink(value).build();
            var actualResult = linkConverterService.getWebUrl(linkDto).getWebUrl();
            Assert.assertEquals(expectedResult, actualResult);
        });

        when(converterFactory.getUrlConverter(any(UriComponents.class)))
                .thenReturn(new SearchUrlConverter());

        searchMap.forEach((expectedResult, value) -> {
            var linkDto = LinkDto.builder().deepLink(value).build();
            var actualResult = linkConverterService.getWebUrl(linkDto).getWebUrl();
            Assert.assertEquals(expectedResult, actualResult);
        });

        when(converterFactory.getUrlConverter(any(UriComponents.class)))
                .thenReturn(new HomeUrlConverter());

        prepareHomeDeepLinks();
        homeMap.forEach((expectedResult, value) -> {
            var linkDto = LinkDto.builder().deepLink(value).build();
            var actualResult = linkConverterService.getWebUrl(linkDto).getWebUrl();
            Assert.assertEquals(expectedResult, actualResult);
        });
    }

    private void prepareProductLinks() {
        this.productMap = new HashMap<>();
        IntStream.range(0, 5).forEach(i -> {
            int productId = 100_000 + (int) (Math.random() * 8_000_000);
            int boutiqueId = 10_000 + (int) (Math.random() * 800_000);
            int merchantId = 10_000 + (int) (Math.random() * 800_000);

            var baseWebUrl = "https://www.trendyol.com/brand/name-p-" + productId;
            var webUrl = baseWebUrl + "?boutiqueId=" + boutiqueId + "&merchantId=" + merchantId;
            var webUrlWithoutBoutique = baseWebUrl + "?merchantId=" + merchantId;
            var webUrlWithoutMerchant = baseWebUrl + "?boutiqueId=" + boutiqueId;

            var baseDeepLink = "ty://?Page=Product&ContentId=" + productId;
            var deepLink = baseDeepLink + "&CampaignId=" + boutiqueId + "&MerchantId=" + merchantId;
            var deepLinkWithoutCampaign = baseDeepLink + "&MerchantId=" + merchantId;
            var deepLinkWithoutMerchant = baseDeepLink + "&CampaignId=" + boutiqueId;

            productMap.put(webUrl, deepLink);
            productMap.put(webUrlWithoutBoutique, deepLinkWithoutCampaign);
            productMap.put(webUrlWithoutMerchant, deepLinkWithoutMerchant);
        });
    }

    private void prepareSearchLinks() {
        this.searchMap = new HashMap<>();
        List<String> queryList = Arrays.asList("elbise", "%C3%BCt%C3%BC", "test", "ŞŞÖ");
        queryList.forEach(q -> {
            var webUrl = "https://www.trendyol.com/sr?q=" + q;
            var deepLink = "ty://?Page=Search&Query=" + q;
            searchMap.put(webUrl, deepLink);
        });
    }

    private void prepareHomeWebUrlLinks() {
        this.homeMap = new HashMap<>();
        List<String> webUrlPathList = Arrays.asList("/Hesabim/Favoriler", "Hesabim/#/Siparisleri", "test");
        webUrlPathList.forEach(p -> {
            var webUrl = "https://www.trendyol.com/" + p;
            var deepLink = "ty://?Page=Home";
            homeMap.put(webUrl, deepLink);
        });
    }

    private void prepareHomeDeepLinks() {
        this.homeMap = new HashMap<>();
        List<String> deepLinkPathList = Arrays.asList("Favorites", "Order", "test");
        deepLinkPathList.forEach(p -> {
            var deepLink = "ty://?Page=" + p;
            var webUrl = "https://www.trendyol.com";
            homeMap.put(webUrl, deepLink);
        });
    }
}
