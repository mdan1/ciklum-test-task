package com.trendyol.exception;

public class InvalidUrlException extends RuntimeException {

    public InvalidUrlException() {
        super("Exception! URL is not valid!");
    }
}
