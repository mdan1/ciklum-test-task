package com.trendyol.model;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@RequiredArgsConstructor
public class LinkPK implements Serializable {

    private String webUrl;
    private String deepLink;
}
