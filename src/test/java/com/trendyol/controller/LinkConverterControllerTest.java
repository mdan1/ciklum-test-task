package com.trendyol.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.services.LinkConverterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LinkConverterController.class)
public class LinkConverterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LinkConverterService service;

    @Test
    public void toDeepLinkTest() throws Exception {
        var link = prepareDeepLink();
        var objectMapper = new ObjectMapper();
        var linkDto = new LinkDto();
        linkDto.setWebUrl("https://www.trendyol.com/butik/liste/erkek");
        when(service.getDeepLink(linkDto)).thenReturn(link);
        this.mockMvc.perform(post("/links/deep-link")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(linkDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("ty://?Page=Home&SectionId=2")));
    }

    @Test
    public void toWebUrlTest() throws Exception {
        var link = prepareWebUrl();
        var objectMapper = new ObjectMapper();
        var linkDto = new LinkDto();
        linkDto.setDeepLink("ty://?Page=Home&SectionId=2");
        when(service.getWebUrl(linkDto)).thenReturn(link);
        this.mockMvc.perform(post("/links/web-url")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(linkDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("https://www.trendyol.com/butik/liste/erkek")));
    }

    private Link prepareWebUrl() {
        var link = new Link();
        link.setWebUrl("https://www.trendyol.com/butik/liste/erkek");
        var date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        link.setDate(date);
        return link;
    }

    private Link prepareDeepLink() {
        var link = new Link();
        link.setDeepLink("ty://?Page=Home&SectionId=2");
        var date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        link.setDate(date);
        return link;
    }

}
