package com.trendyol.services.converter;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static com.trendyol.services.converter.UrlConstants.WEB_URL_TRENDYOL;

/**
 * The root interface for accessing different types of UrlConverter.
 *
 * @author mdan@ciklum.com
 * @see ProductUrlConverter
 * @see SearchUrlConverter
 * @see HomeUrlConverter
 */
public interface UrlConverter {

    /**
     * Converting web-url to deepLink using appropriate UrlConverter.
     *
     * @param uriComponents - contains web-url information.
     * @return String converted deepLink.
     */
    String toDeepLink(UriComponents uriComponents);

    /**
     * Converting deepLink to web-url using appropriate UrlConverter.
     *
     * @param uriComponents - contains deepLink information.
     * @return String converted web-url.
     */
    String toWebUrl(UriComponents uriComponents);

    default String toDeepLink(UriComponentsBuilder uriComponentsBuilder) {
        return uriComponentsBuilder
                .scheme("ty")
                .host("")
                .build()
                .toUriString();
    }

    default String toWebUrl(UriComponentsBuilder uriComponentsBuilder) {
        return uriComponentsBuilder
                .scheme("https")
                .host(WEB_URL_TRENDYOL)
                .build()
                .toUriString();
    }
}
