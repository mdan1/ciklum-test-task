package com.trendyol.controller;

import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.services.LinkConverterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("links")
public class LinkConverterController {

    private static final String LOG_MESSAGE = "Endpoint - {}() call";

    private final LinkConverterService linkConverterService;

    @PostMapping("/deep-link")
    @ResponseStatus(HttpStatus.CREATED)
    public Link toDeepLink(@RequestBody LinkDto webLink) {
        log.info(LOG_MESSAGE, "toDeepLink");
        return linkConverterService.getDeepLink(webLink);
    }

    @PostMapping("/web-url")
    @ResponseStatus(HttpStatus.CREATED)
    public Link toWebUrl(@RequestBody LinkDto deepLink) {
        log.info(LOG_MESSAGE, "toWebUrl");
        return linkConverterService.getWebUrl(deepLink);
    }
}
